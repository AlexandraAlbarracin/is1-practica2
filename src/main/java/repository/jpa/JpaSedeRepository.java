package repository.jpa;

import org.springframework.stereotype.Repository;

import repository.OperationRepository;
import domain.Operation;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Operation, Long> implements
		OperationRepository {
}
