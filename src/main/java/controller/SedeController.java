package controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.AccountService;
import service.SedeService;
import service.TransferService;
import domain.Account;
import domain.Person;
import domain.Sede;
import form.CreateAccountForm;
import form.TransferForm;

@Controller
public class SedeController {

	@Autowired
	SedeService sedeService;


	
	@RequestMapping(value = "/add-sede", method = RequestMethod.GET)
	String addNewSede(@RequestParam String direccion, @RequestParam String nombre_banco,ModelMap model) {
		Sede newsede = direccion == null ? new Sede() : sedeService.agregar(direccion, nombre_banco);
		model.addAttribute("sede", newsede);
		return "add-sede";
	}

	@RequestMapping(value = "/transfer", method = RequestMethod.POST)
	String transfer(@ModelAttribute TransferForm transfer, ModelMap model) {
		transferService.transfer(transfer.getSourceAccountNumber(), transfer.getTargetAccountNumber(), transfer.getAmount());
		return "home";
	}

	@RequestMapping(value = "/transfer", method = RequestMethod.GET)
	String showTransfer(@ModelAttribute TransferForm transfer, ModelMap model) {
		return "transfer";
	}

	@RequestMapping(value = "/account", method = RequestMethod.POST)
	String saveAccount(@ModelAttribute Account account, ModelMap model) {
		accountService.save(account);
		return "account-list";
	}

	@RequestMapping(value = "/account", method = RequestMethod.GET)
	String listAccounts(ModelMap model) {
		model.addAttribute("accounts", accountService.getAccounts());
		return "account-list";
	}

	@RequestMapping(value = "/add-account", method = RequestMethod.GET)
	String addAccount(ModelMap model) {
		return "add-account";
	}
	
	@RequestMapping(value = "/register-account", method = RequestMethod.POST)
	String createAccount(@ModelAttribute CreateAccountForm createAccount, ModelMap model) {
		accountService.createAccount(createAccount.getOwnerIds(), createAccount.getAccount());
		return "add-account";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	String home(ModelMap model) {
		return "home";
	}
}
